## Simple synthetic test load

# Test expansion of definitions.
# Expect "fubble: BAR"
FOO = BAR
BAZ = $(FOO)
fubble: $(BAZ)

# All this stuff should drop out
./Makefile.in:  ./Makefile.am  $(am__configure_deps)
	@for dep in $?; do \
	  case '$(am__configure_deps)' in \
	    *$$dep*) \
	      echo ' cd . && /bin/bash /home/esr/src/giflib-4.1.6/missing --run automake-1.10 --gnu '; \
	      cd . && /bin/bash /home/esr/src/giflib-4.1.6/missing --run automake-1.10 --gnu  \
		&& exit 0; \
	      exit 1;; \
	  esac; \
	done; \
	echo ' cd . && /bin/bash /home/esr/src/giflib-4.1.6/missing --run automake-1.10 --gnu  Makefile'; \
	cd . && \
	  /bin/bash /home/esr/src/giflib-4.1.6/missing --run automake-1.10 --gnu  Makefile
.PRECIOUS: Makefile
Makefile: ./Makefile.in ./config.status
	@case '$?' in \
	  *config.status*) \
	    echo ' /bin/bash ./config.status'; \
	    /bin/bash ./config.status;; \
	  *) \
	    echo ' cd . && /bin/bash ./config.status $@ $(am__depfiles_maybe)'; \
	    cd . && /bin/bash ./config.status $@ $(am__depfiles_maybe);; \
	esac;

./config.status: ./configure $(CONFIG_STATUS_DEPENDENCIES)
	/bin/bash ./config.status --recheck

./configure:  $(am__configure_deps)
	cd . && /bin/bash /home/esr/src/giflib-4.1.6/missing --run autoconf
./aclocal.m4:  ./configure.ac
	cd . && /bin/bash /home/esr/src/giflib-4.1.6/missing --run aclocal-1.10 $(ACLOCAL_AMFLAGS)

config.h: stamp-h1
	@if test ! -f $@; then \
	  rm -f stamp-h1; \
	  $(MAKE) $(AM_MAKEFLAGS) stamp-h1; \
	else :; fi

stamp-h1: ./config.h.in ./config.status
	@rm -f stamp-h1
	cd . && /bin/bash ./config.status config.h
./config.h.in:  $(am__configure_deps) 
	cd . && /bin/bash /home/esr/src/giflib-4.1.6/missing --run autoheader
	rm -f stamp-h1
	touch $@

# Removed stuff ends here
