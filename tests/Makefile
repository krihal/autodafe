# Test makefile for makemake

# SPDX-FileCopyrightText: Copyright Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

# Use absolute path so tests that change working directory still use
# scripts from parent directory.  Note that using $PWD seems to fail
# here under Gitlab's CI environment.
PARDIR=$(realpath ..)
PATH := $(PARDIR):$(realpath .):${PATH}

# Find all *.tst entries to test
TESTLOADS := $(shell ls -1 *.tst | sed '/.tst/s///' | sort)

.PHONY: cHeck clean testlist listcheck buildchecks

check:
	@make tap | ./tapview

.SUFFIXES: .chk

clean:
	rm -fr *~

# Show summary lines for all tests.
testlist:
	@grep '^##' *.tst
listcheck:
	@for f in *.tst; do \
	    if ( head -3 $$f | grep -q '^ *##' ); then :; else echo "$$f needs a description"; fi; \
	done

# Rebuild characterizing tests
buildchecks:
	@for file in $(TESTLOADS); do \
	    echo "Remaking $${file}.chk"; \
	    OPTS=`sed -n /#options:/s///p <$${file}.tst`; \
	    makemake $$OPTS <$${file}.tst >$${file}.chk 2>&1 || exit 1; \
	done;

RUN_TARGETS=$(TESTLOADS:%=run-regress-%)
$(RUN_TARGETS): run-regress-%: %.tst
	@(test=$(<:.tst=); legend=$$(sed -n '/^## /s///p' <"$<" 2>/dev/null || echo "(no description)"); \
	OPTS=`sed -n /#options:/s///p $<`; \
	makemake $$OPTS <$< | ./tapdiffer "$${test}: $${legend}" "$${test}.chk")


TEST_TARGETS = $(RUN_TARGETS)

tap: count $(TEST_TARGETS)
count:
	@echo 1..$(words $(TEST_TARGETS))

