= autodafe

This project collects resources for converting an autotools
build recipe to a bare makefile that can be read and modified
by humans.

The "De-Autocoonfiscation HOWTO" describnes the conversion procedure.

The prinicipal tool, makemake, reduces a generated Makefile to an
equivalent form suitable for human modification and with internal
automake cruft removed. It is intended to be used with ifdex(1) to
enable severing a project from its autotools build recipe, leaving a
bare Makefile in place.  A HOWTO describing a conversion workflow for
an entire project is included.

One other tool is planned but not yet implemented.
